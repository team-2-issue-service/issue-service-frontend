# T2 Issue Service

## Project Description

This application is made for town council members and their constituents to be able to efficiently solve problems within their community. The application offers citizens the ability to view upcoming town hall meetings and to post issues discovered within the town. The council members are able to view these posted issues as well as create, update, and delete town hall meetings.

## Technologies Used

* Node.js - version 14
* Docker
* Google Kubernetes Engine
* Cloud Run
* Cloud Pub/Sub
* Cloud SQL
* GCP Datastore
* GCP Cloud Logging
* BigQuery
* Firebase Hosting

## Features

List of features ready and TODOs for future development
* Authentication service for council members
* Issue ingestion service using pub/sub
* Town meeting scheduler API
* Issue analysis service using Datastore

## Getting Started
   
* In git bash run `git clone https://gitlab.com/jlhillery999/hillery-wedding-service.git`
* In Windows, open command prompt and run `npm i` to install all dependencies
* In the command prompt, run `npm start`. This will host the frontend locally.

## Usage
Top of Page
![Top of Page](./images/toppage.PNG)

Bottom of Page
![Bottom of Page](./images/bottompage.PNG)

* Go to localhost:3000 in your browser to open the webpage locally
* To submit an issue, fill out all of the information in the Issue Submission Form component.
* In order to edit the Meeting Planner table, click the login button in the top right corner and enter the password.
* To create a meeting fill out the information in the "Create or Replace a Meeting" form in the Meeting Planner component and click the "Create Meeting" button.
* To replace a meeting, first click on the meeting you'd like to replace on the table in the Meeting Planner component. Then, fill out the information in the "Create or Replace a Meeting" form and click the "Replace Meeting" button.
* To delete a meeting, click on the meetinf you'd like to delete on the Meeting Planner table. Then, click the "Delete Meeting" button.
* To filter the Issue Analysis chart, first click on the parameter you'd like to filter by at the top of the Issue Analysis Chart component. If the filter requires input, an input field will appear beside the filter parameters. Once it is filled out, click on the filter icon.
* To highlight/unhighlight or mark one or more issues as reviewed, click the checkbox beside the entries you'd like to affect and then click the respective button below the Issue Analysis chart.

## Contributors

* Jarrick Hillery
* James Kirk
* Camden Snyder
* Alan Tillman