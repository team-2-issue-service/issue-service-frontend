import { SyntheticEvent, useRef, useState } from 'react';
import { format } from 'date-fns'
import axios from 'axios';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Button from '@mui/material/Button'
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';


export default function IssueSubmissionForm(){

    const location = useRef<any>('');
    const description = useRef<any>('');

    const [date, setDate] = useState<Date>(new Date());
    const [type, setType] = useState<string>('other');

    const handleTypeChange = (event: SelectChangeEvent) => {
      setType(event.target.value as string);
    };
    
    const handleDateChange = (newDate: Date | null) => {
        setDate(newDate ?? new Date());
    };

    async function submitForm(event:SyntheticEvent){
        const issue = {datePosted: format(new Date(), 'MM-dd-yyyy'), dateIssue:format(date, 'MM-dd-yyyy'), description:description.current.value, location:location.current.value, type:type, reviewed:false, highlighted:false};
        await axios.post('https://issue-ingestion-v12-b7tjvanksa-uc.a.run.app/submitissue', issue)
    }

    return(<Paper elevation={2} 
        component='form'
        sx={{
            '& .MuiFormControl-root': { m: 1, width: 1 },
            '& .MuiTextField-root': { m: 1, width: 1},
            '& .MuiSelect-root': { m: 1, width: 1},
            '& .MuiButton-root': { m: 1, width: 0.5 },
            '& .MuiTypography-root': {fontFamily: "fontFamily"},
            display: 'flex',
            justifyContent: 'center',
            bgcolor: "background.paper",
            height: 665
        }}>
        <Box width={0.9}>
            <br/>
            <Typography variant='h5'>Issue Submission Form</Typography>
            <FormControl>
                <InputLabel variant="outlined" id="issue-type-label">Type of Issue</InputLabel>
                <Select
                    labelId="issue-type-label"
                    id="issue-type"
                    value={type}
                    label="Type of Issue"
                    onChange={handleTypeChange}
                >
                    <MenuItem value={'infrastructure'}>Infrastructure</MenuItem><br/>
                    <MenuItem value={'safety'}>Safety</MenuItem><br/>
                    <MenuItem value={'public_health'}>Public Health</MenuItem><br/>
                    <MenuItem value={'pollution'}>Pollution</MenuItem><br/>
                    <MenuItem value={'noise'}>Noise/Disturbing the peace</MenuItem><br/>
                    <MenuItem value={'other'}>Other</MenuItem>
                </Select>
            
            <TextField
                required
                id="issue_label"
                label="Location"
                variant="outlined"
                inputRef={location}
            />
            <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                    label="Date of Issue"
                    inputFormat="MM/dd/yyyy"
                    value={date}
                    onChange={handleDateChange}
                    renderInput={(params) => <TextField {...params} />}
            />
            </LocalizationProvider>
            <TextField
                required
                multiline
                id="issue_description"
                label="Description"
                variant="outlined"
                inputRef={description}
                rows={12}
            />
            <Button variant='contained' color='primary' onClick={submitForm}>Submit</Button>
            </FormControl>
        </Box>
    </Paper>)
}