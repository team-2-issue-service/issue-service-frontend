import * as React from 'react';
import Popover from '@mui/material/Popover';
import Button from '@mui/material/Button';
import { TextField } from '@mui/material';

export default function LoginPopover() {
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };


    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    const passIn = React.useRef<any>("");

    function login(){
        localStorage.setItem("password", passIn.current.value)
    }

    return (
        <div>
            <Button variant="contained" color="secondary" onClick={handleClick}>
            Login
            </Button>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <TextField
                        required
                        id="password_label"
                        label="Password"
                        variant="outlined"
                        type="password"
                        inputRef={passIn}
                        />
                <div>
                    <Button onClick={login}>Enter</Button>
                </div>
            
            </Popover>
        </div>
    );
}