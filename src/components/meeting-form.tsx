import { Button, FormControl, Paper, TextField, Box, Stack, Typography } from "@mui/material";
import axios from "axios";
import { SyntheticEvent, useState, useRef, useEffect } from "react";
import { DataGrid, GridColDef, GridRowsProp } from "@mui/x-data-grid";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateTimePicker from '@mui/lab/DateTimePicker';
import { format } from 'date-fns'

export default function MeetingForm(){

    const [meetings,setMeetings] = useState([{mID:0, mLocation:'', mDate:'', mTime:'', mTopics:''}]);
    const [rowID, setRowID] = useState(0);
    const [refreshState, setRefreshState] = useState(true);
    const [date, setDate] = useState<Date>(new Date());

    const locationIn = useRef<any>("");
    const topicsIn = useRef<any>("");

    const deleteIn = useRef<any>("");

    function refresh(event:SyntheticEvent){
        setRefreshState(!refreshState);
    }

    async function getMeetings(){
        const response = await axios.get('http://34.74.48.138/meetings');
        setMeetings(response.data);
    }

    async function postMeeting(){
        try{
            const location = locationIn.current.value;
            const newDate = format(date, 'MM-dd-yyyy');
            const time = format(date, 'hh:mm aa');
            const topics = topicsIn.current.value;
            const newMeeting = {mID:0, mLocation:location, mDate:newDate, mTime:time, mTopics:topics}
            await axios.post('http://34.74.48.138/meetings', newMeeting, {headers: {
                'authorization': localStorage.getItem('password')
            }});
        }catch{
            alert("Invalid Password")
        }
    }

    async function putMeeting(){
        try{
            const location = locationIn.current.value;
            const newDate = format(date, 'MM-dd-yyyy');
            const time = format(date, 'hh:mm aa');
            const topics = topicsIn.current.value;
            const id = rowID;
            const newMeeting = {mID:0, mLocation:location, mDate:newDate, mTime:time, mTopics:topics}
            await axios.put(`http://34.74.48.138/meetings/${id}`, newMeeting, {headers: {
                'authorization': localStorage.getItem('password')
            }});
            setRefreshState(!refreshState);
        }catch{
            alert("Invalid Password")
        }
    }

    async function deleteMeeting(){
        if(window.confirm('Are you sure you\'d like to delete this meeting? This action can not be reversed.')){
        try{
            if(deleteIn){
                const meetingID = rowID;
                await axios.delete(`http://34.74.48.138/meetings/${meetingID}`, {headers: {
                    'authorization': localStorage.getItem('password')
                }});
            }else{
                alert("Must provide input")
            }
            setRefreshState(!refreshState);
        }catch{
            alert("Invalid Password")
        }
        }
    }

    function getRowID(event:any){
        setRowID(Number(event[0]))
    }

    const columns:GridColDef[] = [
        {field:'id', hide:true},
        {field:'location', headerName:'Location', minWidth:140},
        {field:'date', headerName:'Date', minWidth:110},
        {field:'time', headerName:'Time', minWidth:110},
        {field:'topics', headerName:'Topics', minWidth:120, flex:1}
    ]

    const rows:GridRowsProp = meetings.map(x => {
        return {id:x.mID, location:x.mLocation, date:x.mDate.replaceAll('-','/'), time:x.mTime, topics:x.mTopics}
    });

    useEffect(()=>{
        getMeetings();
    }, [refreshState])

    return(<Paper elevation={2} 
        component='form'
        sx={{
            '& .MuiTextField-root': { width: '0.3' },
            '& .MuiButton-root': { width: '0.3' },
            '& .MuiTypography-root': {fontFamily: "fontFamily"},
            display: 'flex',
            justifyContent: 'center',
            fontFamily: "fontFamily",
            bgcolor: "background.paper",
            height: 665,
            width: 1
        }}>
            <Box width='0.95'>
                <br/>
                <Typography variant='h5'>Meeting Planner</Typography>
                <br/>
                    <Button color='primary' onClick={refresh}>Show Meetings</Button>
                    <Box height='60%'><DataGrid
                        columns={columns}
                        rows={rows}
                        autoPageSize
                        onSelectionModelChange={getRowID}/></Box>
                    <FormControl size='medium' sx={{width: 1}}>
                    <br/>
                    <Typography variant='h6'>Create or Replace a Meeting</Typography>
                    <Stack spacing={2} direction='row'>
                    <TextField
                    required
                    id="location_label"
                    label="Location"
                    variant="outlined"
                    inputRef={locationIn}
                    />
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DateTimePicker
                            label="Date & Time"
                            value={date}
                            onChange={(newDate) => {
                                setDate(newDate ?? new Date());
                            }}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                    <TextField
                    required
                    id="topics_label"
                    label="Discussion Topics"
                    variant="outlined"
                    inputRef={topicsIn}
                    />
                    </Stack>
                    <br/>
                    <Stack spacing={2} direction='row' sx={{width: 1}}>
                    <Button variant='contained' color='primary' onClick={postMeeting}>Create Meeting</Button>
                    <Button variant='contained' color='primary' onClick={putMeeting}>Replace Meeting</Button>
                    <Button variant='contained' color='secondary' onClick={deleteMeeting}>Delete Meeting</Button>
                    </Stack>
                </FormControl>
            </Box>
    </Paper>)
}
