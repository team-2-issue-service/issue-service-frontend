import { createTheme } from '@mui/material/styles';
import "@fontsource/montserrat";

export const theme = createTheme({
  palette: {
    primary: {
      main: '#795548',
    },
    secondary: {
      main: '#ef6c00',
    },
    background: {
        default: '#fff7f6',
        paper: '#ffffff',
    },
    error: {
      main: '#d50000',
    },
  },
  typography: {
    fontFamily: 'Montserrat',
  },
});