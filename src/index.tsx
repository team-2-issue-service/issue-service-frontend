import React from 'react';
import ReactDOM from 'react-dom';
import Box from '@mui/material/Box';
import IssueSubmissionForm from './components/issue-submission-form'
import IssueAnalysisPaper from './components/issue-analysis-paper'
import ThemeProvider from '@mui/material/styles/ThemeProvider'
import { theme } from './theme'
import MeetingForm from './components/meeting-form';
import AppBar from '@mui/material/AppBar'
import { Typography, Toolbar } from '@mui/material'
import LoginPopover from './components/login-popover'
import Grid from '@mui/material/Grid'

ReactDOM.render(
  <React.StrictMode>
      <ThemeProvider theme={theme}>
        <Box bgcolor="background.default">
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <AppBar position="static">
                <Toolbar>
                  <Typography variant="h4" sx={{ flexGrow: 1, fontFamily: "fontFamily" }}>
                    Townhall Meetings
                  </Typography>
                  <LoginPopover/>
                </Toolbar>
              </AppBar>
            </Grid>
            <Grid item xs={4}>
              <IssueSubmissionForm/>
            </Grid>
            <Grid item xs={8}>
              <MeetingForm/>
            </Grid>
            <Grid item xs={12}>
              <IssueAnalysisPaper/>
            </Grid>
          </Grid>
        </Box>       
      </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);